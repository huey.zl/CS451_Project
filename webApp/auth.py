from flask import Blueprint
#Blueprints have a bunch of roots and URLs stored inside of it
#It is a way for us to separate our app out

auth = Blueprint('auth', __name__)

@auth.route('/login')
def login():
    return "<p>Login</p>"

@auth.route('/logout')
def logout():
    return "<p>logout</p>"


