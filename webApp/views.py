#this is going to store all of the standard routes of our site, where users can go to like homepage etc.

from flask import Blueprint
#Blueprints have a bunch of routes and URLs stored inside of it
#It is a way for us to separate our app out

views = Blueprint('views', __name__)

@views.route('/')
def home():
    return "<h1>Test</h1>"

@views.route('/dashboard')
def dashboard():
    return "<p>User Dashboard Here</p>"

